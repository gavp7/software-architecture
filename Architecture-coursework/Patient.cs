﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Architecture_coursework
{

    public class Patient
    {
        public string name { get; set; }
        public string nhs_no { get; set; }
        public string address { get; set; }
        public string condition { get; set; }
        public int hospital { get; set; }
        public DateTime date { get; set; }
        public string actions { get; set; }

        public Patient(string name, string nhs_no, string address, string condition, int hospital, string actions, DateTime date)
        {
            this.name = name;
            this.nhs_no = nhs_no;
            this.address = address;
            this.condition = condition;
            this.hospital = hospital;
            if (date == null)
            {
                this.date = DateTime.Now;
            } else
            {
                this.date = date;
            }
            this.actions = actions;
        }
    }
}
