﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Architecture_coursework
{
    /// <summary>
    /// Interaction logic for AmbulanceInterface.xaml
    /// </summary>
    public partial class AmbulanceInterface : Window
    {
        private PatientLogic patientLogic;
        private Patient patient;
        public AmbulanceInterface(Patient patient, PatientLogic p)
        {
            InitializeComponent();
            this.patientLogic = p;
            this.patient = patient;
            txtdetails.Text = patient.name + "\n" + patient.nhs_no + "\n" + patient.address + "\n" + patient.condition;
            
        }

        private void btnfinish_Click(object sender, RoutedEventArgs e)
        {
            storeDetails();
        }
       private void storeDetails()
        {
            patientLogic.updatePatient(patient.name, patient.nhs_no, patient.address, patient.condition, patient.hospital, txtaction.Text, patient.date);
            patientLogic.storePatient(patient.nhs_no);
            MessageBox.Show("Patient Details Stored");
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            PatientHistory ph = new PatientHistory(patient.nhs_no, patientLogic);
            ph.Show();
        }
    }
}
