﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Architecture_coursework
{
    /// <summary>
    /// Interaction logic for HospitalInterface.xaml
    /// </summary>
    public partial class HospitalInterface : Window


    {
        private PatientLogic patientLogic;
        private Dictionary<string, Patient> patients = new Dictionary<string, Patient>();
        public HospitalInterface(PatientLogic _patientLogic)
        {
            InitializeComponent();
        patientLogic = _patientLogic;
            for (int i = 0; i < 21; i++)
            {
                combohospital.Items.Add(i);
            }
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            addPatients();
        }
      
        private void lstpatients_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            getPatient();
        }
       

        private void btnambulance_Click(object sender, RoutedEventArgs e)
        {
            getAmbulance();
        }

        private void getPatient()
        {
            if (lstpatients.SelectedValue != null)
            {
                string temp = lstpatients.SelectedValue.ToString();
                txtpatient.Text = patientLogic.getPatient(temp);
            }
        }
        private void getAmbulance()
        {
            if (lstpatients.SelectedValue != null)
            {
                Patient p = patients[lstpatients.SelectedItem.ToString()];
                AmbulanceInterface a = new AmbulanceInterface(p, patientLogic);
                a.Show();
            }
        }
        private void btnhistory_Click(object sender, RoutedEventArgs e)
        {
            PatientHistory ph = new PatientHistory(lstpatients.SelectedItem.ToString(), patientLogic);
            ph.Show();
        }

        private void addPatients()
        {

            lstpatients.Items.Clear();
            txtpatient.Text = "";
            patients = patientLogic.getPatients(combohospital.SelectedIndex);
            foreach (KeyValuePair<string, Patient> patient in patients)
            {
                lstpatients.Items.Add(patient.Key);
            }
        }
    }

}
