﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Architecture_coursework
{
    /// <summary>
    /// Interaction logic for PatientHistory.xaml
    /// </summary>
    public partial class PatientHistory : Window
    {
        private string nhs_no;
        private PatientLogic patientLogic;
        private Dictionary<string, Patient> history;
        public PatientHistory(string nhs_no, PatientLogic p)
        {
            InitializeComponent();
            this.nhs_no = nhs_no;
            this.patientLogic = p;
            history = p.getHistory(nhs_no);
            lsthistory.Items.Add("Patient: " + nhs_no);
            foreach (KeyValuePair<string, Patient> entry in history)               
                lsthistory.Items.Add("Date: " + entry.Value.date + "  |  Condition: " + entry.Value.condition + "  |  Actions taken: " + entry.Value.actions);
        }
    }
}
