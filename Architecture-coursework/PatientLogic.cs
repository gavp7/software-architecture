﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Architecture_coursework
{
   public class PatientLogic
    {
        private DataLayer dataLayer = new DataLayer();

        public PatientLogic()
        {

        }
           
        public void storePatient(string nhs_no)
        {
            dataLayer.storePatient(nhs_no);
        }
        public string addPatient(string name, string nhs_no, string address, string condition, int hospital, string action)
        {
            Patient patient = new Patient(name, nhs_no, address, condition, hospital, action, DateTime.Now);
            Boolean success = dataLayer.addPatient(patient);
            if (success)
            {
                return "Patient " + nhs_no + " added";
            }
            else
            {
                return "Failed to add patient " + nhs_no;
            }
        }
        public string getPatient(string nhs_no)
        {
            Patient patient = dataLayer.getPatient(nhs_no);
            if (patient != null)
            {
                return patient.nhs_no + "\n" + patient.name + "\n" + patient.address + "\n" + patient.condition;
            }
            else
            {
                return "Patient " + nhs_no + " does not exist";
            }
        }
        public Dictionary<string, Patient> getPatients(int hospital)
        {
            Dictionary<string, Patient> tempPatients = dataLayer.getPatients();
            Dictionary<string, Patient> returnPatients = new Dictionary<string, Patient>();
            foreach (KeyValuePair<string, Patient> patient in tempPatients)
            {
                if (patient.Value.hospital == hospital)
                {
                    returnPatients.Add(patient.Key, patient.Value);
                }
            }
            return returnPatients;
        }
        public string removePatient(string nhs_no)
        {
            Boolean success = dataLayer.removePatient(nhs_no);
            if (success)
            {
                return "Patient " + nhs_no + " removed";
            }
            else
            {
                return "Patient " + nhs_no + " not removed";
            }
        }
        public Dictionary<string, Patient> getHistory(string nhs_no)
        {
            return dataLayer.getHistory(nhs_no);
        }

        public string updatePatient(string name, string nhs_no, string address, string condition, int hospital, string actions, DateTime date)
        {
            Patient patient = new Patient(name, nhs_no, address, condition, hospital, actions, date);
            Boolean success = dataLayer.updatePatient(nhs_no, patient);
            if (success)
            {
                return "Patient " + nhs_no + " successfully updated";
            }
            else
            {
                return "Patient " + nhs_no + " not updated";
            }
        }
    }
}
