﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Architecture_coursework
{
    public class DataLayer
    {
        private Dictionary<string, Patient> patients = new Dictionary<string, Patient>();
        private SqlConnection thisConnection;
        public DataLayer() {
          //  Connect();
        }
        public Dictionary<string, Patient> getHistory(string nhs_no)
        {
            Dictionary<string, Patient> tempList = new Dictionary<string, Patient>();
            using (thisConnection = new SqlConnection("Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename = \"C:\\Users\\gavin\\documents\\visual studio 2015\\Projects\\Architecture-coursework\\Architecture-coursework\\Database.mdf\"; Integrated Security = True"))
            {
                string getpatients = "Select nhs_no, name, address, condition, date, actions from dbo.patients where nhs_no = " + nhs_no;
                using (SqlCommand cmd = new SqlCommand(getpatients, thisConnection))
                {
                    thisConnection.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        // Check is the reader has any rows at all before starting to read.
                        if (reader.HasRows)
                        {
                            // Read advances to the next row.
                            while (reader.Read())
                            {
                                string name = reader.GetString(reader.GetOrdinal("name"));
                                string nhs = reader.GetString(reader.GetOrdinal("nhs_no"));
                                string address = reader.GetString(reader.GetOrdinal("address"));
                                string condition = reader.GetString(reader.GetOrdinal("condition"));
                                string actions = reader.GetString(reader.GetOrdinal("actions"));
                                DateTime date = reader.GetDateTime(reader.GetOrdinal("date"));


                                Patient p = new Patient(name, nhs, address, condition, 0, actions, date);
                                
                                tempList.Add(condition, p);
                            }
                        }
                    }
                    }
                }
            return tempList;
        }

        public void storePatient(string nhs_no) //Store the patient in the database
        {
            Patient p = getPatient(nhs_no);
            using (thisConnection = new SqlConnection("Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename = \"C:\\Users\\gavin\\documents\\visual studio 2015\\Projects\\Architecture-coursework\\Architecture-coursework\\Database.mdf\"; Integrated Security = True"))
            {
                string save = "INSERT INTO dbo.patients (nhs_no,name,address,condition,date,actions) VALUES (@nhs_no,@name,@address,@condition,@date,@actions)";
                using (SqlCommand insertpatient = new SqlCommand(save))
                {
                    insertpatient.Connection = thisConnection;
                    insertpatient.Parameters.Add("@nhs_no", SqlDbType.VarChar, 50).Value = p.nhs_no;
                    Console.WriteLine(p.nhs_no);
                    insertpatient.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = p.name;
                    insertpatient.Parameters.Add("@address", SqlDbType.VarChar, 50).Value = p.address;
                    insertpatient.Parameters.Add("@condition", SqlDbType.VarChar).Value = p.condition;
                    insertpatient.Parameters.Add("@date", SqlDbType.Date).Value = p.date;
                    insertpatient.Parameters.Add("@actions", SqlDbType.VarChar).Value = p.actions;
                    thisConnection.Open();
                    insertpatient.ExecuteNonQuery();
                    thisConnection.Close();
                }
            }
        }

    
        public Boolean addPatient(Patient patient) //add new patient
        {
            if(patients.ContainsKey(patient.nhs_no))
            {
                //patient already exists
                return false;
            }
            else
            {
                //add patient to data store
                patients.Add(patient.nhs_no, patient);
                return true;
            }
        }

        public Patient getPatient(string nhs_no) //retrieve patient
        {
            Patient patient = patients[nhs_no];
            Console.WriteLine(patient.name);
            return patient;
        }

        public Boolean removePatient(string nhs_no) //remove patient
        {
            if(patients.ContainsKey(nhs_no))
            {
                patients.Remove(nhs_no);
                return true;
            }
            else
            {
                return false;
            }
        }
        public Dictionary<string, Patient> getPatients()
        {
            return patients;
        }
        public Boolean updatePatient(string nhs_no, Patient patient)
        {
            if(patients.ContainsKey(nhs_no))
            {
                Patient update = patients[nhs_no];
                update.name = patient.name;
                update.address = patient.address;
                update.condition = patient.condition;
                update.actions = patient.actions;

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
