﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Architecture_coursework
{
    /// <summary>
    /// Interaction logic for OperatorInterface.xaml
    /// </summary>
    public partial class OperatorInterface : Window
    {
        private PatientLogic patientLogic;

        public OperatorInterface(PatientLogic _patientLogic)
        {
            InitializeComponent();
            patientLogic = _patientLogic;
        }

      
        private void btnsubmit_Click(object sender, RoutedEventArgs e)
        {
            Submit();
        }
        

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            txtname.Text = "";
            txtno.Text = "";
            txtcondition.Text = "";
            txtaddress.Text = "";
        }
        private void Submit()
        {
            Random rnd = new Random();
            int hospital = rnd.Next(0, 20);
            MessageBox.Show("Patient details sent to hospital " + (hospital));
            patientLogic.addPatient(txtname.Text, txtno.Text, txtaddress.Text, txtcondition.Text, hospital, null);
            Console.WriteLine(patientLogic.getPatient(txtno.Text));
        }
    }
}
