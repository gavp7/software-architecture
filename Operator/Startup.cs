﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Operator.Startup))]
namespace Operator
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
